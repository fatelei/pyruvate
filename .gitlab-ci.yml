image: 'rustlang/rust:nightly'

stages:
  - test
  - build

variables:
  CARGO_HOME: $CI_PROJECT_DIR/cargo
  APT_CACHE_DIR: $CI_PROJECT_DIR/apt

before_script:
  - apt-get update -yq
  - apt-get install -o dir::cache::archives="$APT_CACHE_DIR" -y python3-dev libcurl4-openssl-dev libelf-dev libdw-dev cmake gcc binutils-dev libiberty-dev

test:
  stage: test
  script:
    - rustc --version
    - cargo --version
    - python3 -V
    - cargo test --no-default-features --verbose -- --nocapture
  except:
    - tags

coverage:
  stage: test
  script:
    - rustc --version
    - cargo --version
    - python3 -V
    - cargo install cargo-kcov
    - export CARGO_INCREMENTAL=0
    - export RUSTFLAGS="-Ccodegen-units=1 -Cinline-threshold=0 -Clink-dead-code -Coverflow-checks=off"
    - |
      wget https://github.com/SimonKagstrom/kcov/archive/master.tar.gz &&
      tar xzf master.tar.gz &&
      cd kcov-master &&
      mkdir build &&
      cd build &&
      cmake .. &&
      make &&
      make install DESTDIR=../../kcov-build &&
      cd ../.. &&
      rm -rf kcov-master &&
      cargo kcov --kcov ./kcov-build/usr/local/bin/kcov --verbose -- --include-path=src --exclude-pattern=/.cargo,/usr/lib,src/pymodule.rs --exclude-region="#[cfg(test)]:#[cfg(testkcovstopmarker)]" --verify &&
      curl https://keybase.io/codecovsecurity/pgp_keys.asc | gpg --no-default-keyring --keyring trustedkeys.gpg --import &&
      curl -Os https://uploader.codecov.io/latest/linux/codecov &&
      curl -Os https://uploader.codecov.io/latest/linux/codecov.SHA256SUM &&
      curl -Os https://uploader.codecov.io/latest/linux/codecov.SHA256SUM.sig &&
      gpgv codecov.SHA256SUM.sig codecov.SHA256SUM &&
      shasum -a 256 -c codecov.SHA256SUM &&
      chmod +x codecov &&
      ./codecov -t ${CODECOV_TOKEN} &&
      echo "Uploaded code coverage"
  allow_failure: true
  except:
    - tags

lint:
  stage: test
  image: 'rust:latest'
  script:
    - rustc --version
    - cargo --version
    - rustup component add clippy rustfmt
    - rustfmt --check src/*.rs
    - cargo clippy
  allow_failure: true
  except:
    - tags

tox:
  stage: test
  image: 'ubuntu:focal'
  before_script:
    - apt-get update -y
    - apt-get install -y software-properties-common
    - add-apt-repository ppa:deadsnakes/ppa
    - apt-get update -yq
    - apt-get install -o dir::cache::archives="$APT_CACHE_DIR" -y python3-dev python3-pip python3-venv python3.6 python3.6-dev python3.6-venv python3.7 python3.7-dev python3.7-venv python3.8 python3.8-dev python3.8-venv python3.9 python3.9-dev python3.9-venv python3.10 python3.10-dev python3.10-venv gcc curl
    - curl https://sh.rustup.rs -sSf | sh -s -- -y
    - PATH="/builds/tschorr/pyruvate/cargo/bin:$PATH" rustup install nightly
    - PATH="/builds/tschorr/pyruvate/cargo/bin:$PATH" rustup default nightly
  script:
    - python3.7 -m venv toxenv
    - toxenv/bin/pip install setuptools-rust tox
    - PATH="/builds/tschorr/pyruvate/cargo/bin:$PATH" toxenv/bin/tox
  except:
    - tags

wheel:
  stage: build
  image: quay.io/pypa/manylinux2010_x86_64
  before_script:
    - curl https://sh.rustup.rs -sSf | sh -s -- -y
  script:
    - PATH="/builds/tschorr/pyruvate/cargo/bin:$PATH" /opt/python/cp37-cp37m/bin/python -m venv py37
    - PATH="/opt/python/cp37-cp37m/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" which python
    - PATH="/opt/python/cp37-cp37m/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" which python3
    - PATH="/opt/python/cp37-cp37m/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" /builds/tschorr/pyruvate/py37/bin/pip wheel -w /builds/tschorr/pyruvate/dist -e .
    - rm -rf build pyruvate.egg-info
    - PATH="/builds/tschorr/pyruvate/cargo/bin:$PATH" /opt/python/cp36-cp36m/bin/python -m venv py36
    - PATH="/opt/python/cp36-cp36m/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" which python
    - PATH="/opt/python/cp36-cp36m/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" which python3
    - PATH="/opt/python/cp36-cp36m/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" /builds/tschorr/pyruvate/py36/bin/pip wheel -w /builds/tschorr/pyruvate/dist -e .
    - rm -rf build pyruvate.egg-info
    - PATH="/builds/tschorr/pyruvate/cargo/bin:$PATH" /opt/python/cp38-cp38/bin/python -m venv py38
    - PATH="/opt/python/cp38-cp38/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" which python
    - PATH="/opt/python/cp38-cp38/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" which python3
    - PATH="/opt/python/cp38-cp38/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" /builds/tschorr/pyruvate/py38/bin/pip wheel -w /builds/tschorr/pyruvate/dist -e .
    - rm -rf build pyruvate.egg-info
    - PATH="/builds/tschorr/pyruvate/cargo/bin:$PATH" /opt/python/cp39-cp39/bin/python -m venv py39
    - PATH="/opt/python/cp39-cp39/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" which python
    - PATH="/opt/python/cp39-cp39/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" which python3
    - PATH="/opt/python/cp39-cp39/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" /builds/tschorr/pyruvate/py39/bin/pip wheel -w /builds/tschorr/pyruvate/dist -e .
    - rm -rf build pyruvate.egg-info
    - PATH="/builds/tschorr/pyruvate/cargo/bin:$PATH" /opt/python/cp310-cp310/bin/python -m venv py310
    - PATH="/opt/python/cp310-cp310/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" which python
    - PATH="/opt/python/cp310-cp310/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" which python3
    - PATH="/opt/python/cp310-cp310/bin:/builds/tschorr/pyruvate/cargo/bin:$PATH" /builds/tschorr/pyruvate/py310/bin/pip wheel -w /builds/tschorr/pyruvate/dist -e .
  artifacts:
    paths:
      - /builds/tschorr/pyruvate/dist
    expire_in: 10 days
  allow_failure: true
  except:
    - tags
